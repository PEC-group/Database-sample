package com.example.databasesample;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.databasesample.database.ContactsDao;
import com.example.databasesample.model.Contact;


public class UpdateContactActivity extends ActionBarActivity implements View.OnClickListener {

    private EditText editTextContactId, editTextContactName, editTextContactPhone;
    private Button buttonUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_contact);

        editTextContactId = (EditText) findViewById(R.id.edittext_contact_id);
        editTextContactName = (EditText) findViewById(R.id.edittext_contact_name);
        editTextContactPhone = (EditText) findViewById(R.id.edittext_contact_phone);
        buttonUpdate = (Button) findViewById(R.id.button_update);

        buttonUpdate.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_update_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        // get the data to update
        Contact contact = new Contact();
        contact.setId(Integer.parseInt(editTextContactId.getText().toString()));
        contact.setName(editTextContactName.getText().toString());
        contact.setPhone(editTextContactPhone.getText().toString());

        ContactsDao contactsDao = new ContactsDao(this);
        boolean isUpdated = contactsDao.updateContact(contact);

        if (isUpdated) {
            Toast.makeText(this, "Contact updated successfully", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Contact not found or could not be updated", Toast.LENGTH_LONG).show();
        }
    }
}
