package com.example.databasesample;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.databasesample.database.ContactsDao;
import com.example.databasesample.model.Contact;


public class AddContactActivity extends ActionBarActivity {

    private EditText editTextName, editTextPhone;
    private Button buttonAddContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        editTextName = (EditText) findViewById(R.id.edittext_name);
        editTextPhone = (EditText) findViewById(R.id.edittext_phone);
        buttonAddContact = (Button) findViewById(R.id.button_add_contact);
        buttonAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveContactInDatabase();
            }
        });
    }

    /**
     * Fetch the user input and insert into the database using the Data Access Object
     */
    private void saveContactInDatabase() {
        Contact contact = new Contact();
        contact.setName(editTextName.getText().toString());
        contact.setPhone(editTextPhone.getText().toString());

        ContactsDao contactsDao = new ContactsDao(this);
        boolean isSuccess = contactsDao.addContact(contact);

        if (isSuccess) {
            Toast.makeText(this, "Contact successfully added", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Failed to add contact", Toast.LENGTH_LONG).show();
        }
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
