package com.example.databasesample;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    private Button buttonAddContact, buttonViewContacts, buttonFindContact, buttonDeleteContact,
            buttonUpdateContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonAddContact = (Button) findViewById(R.id.button_add_contact);
        buttonViewContacts = (Button) findViewById(R.id.button_view_contacts);
        buttonFindContact = (Button) findViewById(R.id.button_find_contacts);
        buttonDeleteContact = (Button) findViewById(R.id.button_delete_contact);
        buttonUpdateContact = (Button) findViewById(R.id.button_update_contact);

        buttonAddContact.setOnClickListener(this);
        buttonViewContacts.setOnClickListener(this);
        buttonFindContact.setOnClickListener(this);
        buttonDeleteContact.setOnClickListener(this);
        buttonUpdateContact.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View clickedButton) {
        Intent intent = new Intent();
        switch (clickedButton.getId()) {
            case R.id.button_add_contact:
                intent.setClass(MainActivity.this, AddContactActivity.class);
                break;
            case R.id.button_view_contacts:
                intent.setClass(MainActivity.this, ViewContactsActivity.class);
                break;
            case R.id.button_find_contacts:
                intent.setClass(MainActivity.this, FindContactActivity.class);
                break;
            case R.id.button_delete_contact:
                intent.setClass(MainActivity.this, DeleteContactActivity.class);
                break;
            case R.id.button_update_contact:
                intent.setClass(MainActivity.this, UpdateContactActivity.class);
                break;
        }
        startActivity(intent);
    }
}
