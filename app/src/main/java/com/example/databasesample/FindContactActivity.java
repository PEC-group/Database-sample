package com.example.databasesample;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.databasesample.database.ContactsDao;
import com.example.databasesample.model.Contact;


public class FindContactActivity extends ActionBarActivity implements View.OnClickListener {

    private EditText editTextFindContact;
    private Button buttonFind;
    private TextView textViewSearchResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_contact);

        editTextFindContact = (EditText) findViewById(R.id.edittext_contact_id);
        buttonFind = (Button) findViewById(R.id.button_find);
        textViewSearchResult = (TextView) findViewById(R.id.textview_search_result);

        buttonFind.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_find_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        // get the id to search and convert it to int
        int contactIdToSearch = Integer.parseInt(editTextFindContact.getText().toString());

        // create the object of ContactsDao to query from database
        ContactsDao contactsDao = new ContactsDao(this);
        Contact contact = contactsDao.getContact(contactIdToSearch);

        // check if contact returned is null or not
        // we are returning null from the .getContact() method if no matching contact if found
        if (contact == null) {
            // show user message that no contacts was found
            textViewSearchResult.setText("No contacts found with given id");
        } else {
            String resultToShow = "Name : " + contact.getName() + "\nPhone : " + contact.getPhone();
            textViewSearchResult.setText(resultToShow);
        }
    }
}
