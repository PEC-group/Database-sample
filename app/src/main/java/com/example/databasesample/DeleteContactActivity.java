package com.example.databasesample;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.databasesample.database.ContactsDao;


public class DeleteContactActivity extends ActionBarActivity implements View.OnClickListener {
    private EditText editTextContactId;
    private Button buttonFind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_contact);

        editTextContactId = (EditText) findViewById(R.id.edittext_contact_id);
        buttonFind = (Button) findViewById(R.id.button_delete);

        buttonFind.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_delete_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        // get the id to delete from editText and convert it to int
        int contactIdToDelete = Integer.parseInt(editTextContactId.getText().toString());

        // create the object of ContactsDao to query from database
        ContactsDao contactsDao = new ContactsDao(this);
        boolean isDeleted = contactsDao.deleteContact(contactIdToDelete);

        if (isDeleted) {
            Toast.makeText(this, "Contact successfully deleted.", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Contact with given could not be found/deleted", Toast.LENGTH_LONG).show();
        }
    }
}
