package com.example.databasesample.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.databasesample.model.Contact;

import java.util.ArrayList;
import java.util.List;

/**
 * Data Access Object for Contacts
 */
public class ContactsDao {

    // table name
    private static final String TABLE_CONTACTS = "contacts";

    // contact id column name
    private static final String COLUMN_CONTACT_ID = "id";
    // contact name column name
    private static final String COLUMN_CONTACT_NAME = "name";
    // contact phone column name
    private static final String COLUMN_CONTACT_PHONE = "phone";

    // SQL statement to create contact table
    static final String CREATE_TABLE_CONTACT = "CREATE TABLE "
            + TABLE_CONTACTS + " ( "
            + COLUMN_CONTACT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_CONTACT_NAME + " TEXT, "
            + COLUMN_CONTACT_PHONE + " TEXT )";

    // SQL statement to drop contact table
    static final String DROP_TABLE_CONTACT = "DROP TABLE IF EXISTS " + TABLE_CONTACTS;

    // column array contacts
    private static final String[] COLUMNS = {COLUMN_CONTACT_ID,
            COLUMN_CONTACT_NAME, COLUMN_CONTACT_PHONE};

    // object of Database helper to access database
    private DbHelper dbHelper;

    /**
     * Constructor to initialize the database helper
     *
     * @param context context to initialize the helper
     */
    public ContactsDao(Context context) {
        dbHelper = new DbHelper(context);
    }

    /**
     * Method to add contact to the database
     *
     * @param contact Contact to add to database
     * @return true if successfully inserted else false
     */
    public boolean addContact(Contact contact) {
        // get the writable database to write the contact into
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // put the contact details in ContentValue
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_CONTACT_NAME, contact.getName());
        cv.put(COLUMN_CONTACT_PHONE, contact.getPhone());

        // insert the contact into database
        long rowId = db.insert(TABLE_CONTACTS, null, cv);

        // close database when work done
        db.close();

        // row id is returned by db.insert() method
        // if row id = -1 the contact was not inserted and return false, return true otherwise
        return rowId != -1;
        /*boolean success;
        if (rowId == -1) {
            success = false;
        } else {
            success = true;
        }
        return success;*/
    }

    /**
     * Get all the contacts in the database
     *
     * @return list of all contacts
     */
    public List<Contact> getContacts() {
        // list to hold the contacts
        List<Contact> contactList = new ArrayList<Contact>();

        // get the database from the helper to read from
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        // cursor to hold the queried result containing the list of contacts
        Cursor cursor = db.query(TABLE_CONTACTS, COLUMNS, null, null, null, null, null, null);

        // Contact object to temporarily hold the contact during each loop
        Contact contact;

        if (cursor.moveToFirst()) {
            do {
                contact = new Contact();
                contact.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_CONTACT_ID)));
                contact.setName(cursor.getString(cursor.getColumnIndex(COLUMN_CONTACT_NAME)));
                contact.setPhone(cursor.getString(cursor.getColumnIndex(COLUMN_CONTACT_PHONE)));

                contactList.add(contact);
            } while (cursor.moveToNext());
        }

        // close the cursor
        cursor.close();

        // close the database
        db.close();

        return contactList;
    }

    /**
     * Get the contact with the given name from the database
     *
     * @param id id of contact to search in database
     * @return the Contact if found else null
     */
    public Contact getContact(int id) {
        // get database to read
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        // run the query and get the result in cursor
        Cursor cursor = db.query(TABLE_CONTACTS, COLUMNS, COLUMN_CONTACT_ID + " = ?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        Contact contact = null;
        // check if cursor is null and cursor has some data
        if (cursor != null && cursor.moveToFirst()) {
            contact = new Contact();
            contact.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_CONTACT_ID)));
            contact.setName(cursor.getString(cursor.getColumnIndex(COLUMN_CONTACT_NAME)));
            contact.setPhone(cursor.getString(cursor.getColumnIndex(COLUMN_CONTACT_PHONE)));

            cursor.close();
        }
        db.close();

        return contact;
    }

    /**
     * Delete a contact from the database
     *
     * @param id id of contact to delete
     * @return true if contact is deleted else false
     */
    public boolean deleteContact(int id) {
        // get the writable database
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        int rowAffected = db.delete(TABLE_CONTACTS, COLUMN_CONTACT_ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();

        return rowAffected != 0;
    }

    /**
     * update the given contact
     *
     * @param contact contact to update
     * @return true if updated else false
     */
    public boolean updateContact(Contact contact) {
        // get the writable database to update the contact
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // ContentValues to put contact data into
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_CONTACT_NAME, contact.getName());
        cv.put(COLUMN_CONTACT_PHONE, contact.getPhone());

        // update the contact int database
        long rowAffected = db.update(TABLE_CONTACTS, cv, COLUMN_CONTACT_ID + " = ?",
                new String[]{String.valueOf(contact.getId())});

        // close database when work done
        db.close();

        return rowAffected != 0;
    }
}
