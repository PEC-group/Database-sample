package com.example.databasesample.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Helper class for creating, upgrading database.
 */
public class DbHelper extends SQLiteOpenHelper {

    // database version
    private static final int DATABASE_VERSION = 1;
    // database name
    private static final String DATABASE_NAME = "Contact-DB";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // run the query to create Contact Database
        // query is obtained from the ContactsDao class
        db.execSQL(ContactsDao.CREATE_TABLE_CONTACT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // drop contact table if it already exists
        db.execSQL(ContactsDao.DROP_TABLE_CONTACT);
        this.onCreate(db);
    }
}
