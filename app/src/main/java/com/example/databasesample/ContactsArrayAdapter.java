package com.example.databasesample;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.databasesample.model.Contact;

import java.util.List;

/**
 * ArrayAdapter for showing contacts list in the ListView
 */
public class ContactsArrayAdapter extends ArrayAdapter<Contact> {

    private Context context;
    private int resourceId;
    private List<Contact> contactList;

    public ContactsArrayAdapter(Context context, int resource, List<Contact> contactList) {
        super(context, resource, contactList);
        this.context = context;
        this.resourceId = resource;
        this.contactList = contactList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textViewContact;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(resourceId, parent, false);

            textViewContact = (TextView) convertView.findViewById(R.id.textview_contact);

            convertView.setTag(textViewContact);
        } else {
            textViewContact = (TextView) convertView.getTag();
        }
        String textToDisplay = "Id : " + getItem(position).getId()
                + "\nName : " + getItem(position).getName()
                + "\nPhone: " + getItem(position).getPhone();
        textViewContact.setText(textToDisplay);

        return convertView;
    }

    @Override
    public Contact getItem(int position) {
        return contactList.get(position);
    }
}
