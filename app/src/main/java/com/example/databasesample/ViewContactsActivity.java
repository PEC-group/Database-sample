package com.example.databasesample;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.databasesample.database.ContactsDao;
import com.example.databasesample.model.Contact;

import java.util.List;


public class ViewContactsActivity extends ActionBarActivity {

    private ListView listViewContact;

    private List<Contact> contactList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_contacts);

        listViewContact = (ListView) findViewById(R.id.listview_contact);

        contactList = getContactListFromDb();
        ContactsArrayAdapter contactArrayAdapter = new ContactsArrayAdapter(
                this, R.layout.layout_single_row, contactList);

        listViewContact.setAdapter(contactArrayAdapter);
    }

    private List<Contact> getContactListFromDb() {
        ContactsDao contactsDao = new ContactsDao(this);
        return contactsDao.getContacts();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_contacts, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
